# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class TalkEvent(Event2):
    NAME = "talk"

    def perform(self):
        if not self.actor.can_see():
            self.add_prop("cannot-see")
            return self.look_failed()
        self.buffer_clear()
        self.buffer_inform("talk.actor", object=self.object)
        self.actor.send_result(self.buffer_get())


    def look_failed(self):
        self.fail()
        self.buffer_clear()
        self.buffer_inform("look.failed.actor")
        self.actor.send_result(self.buffer_get())
