# -*- coding: utf-8 -*-
# Copyleft  2015 JEAN & LENOIR, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import WakeUpEvent
from mud.models.mixins.containing import Containing
from mud.models                   import Player
import mud.game

class WakeUpAction(Action2):
    EVENT = WakeUpEvent
    ACTION = "wake-up"
    
    def resolve_object(self):
            world = mud.game.GAME.world
            self.object="1ere_salle_prison-000"
            loc = world.get(self.object)
            if loc:
                locs = [loc]
            else:
                locs = []
                for k,v in world.items():
                    if isinstance(v, Containing) and \
                       not isinstance(v, Player) and \
                       k.find(self.object) != -1:
                        locs.append(v)
            return locs    